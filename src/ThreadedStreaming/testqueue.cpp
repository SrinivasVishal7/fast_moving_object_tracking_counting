#include "queue.h"
#include <thread>
#include <unistd.h>

void grabFrame( Queue<int>& q )
{
    int count = 0;

    while(1)
    {
        q.push(count);

        if( count == 2000 )
            break;

        count++;
    }
}

void showFrame( Queue<int>& q )
{
    int count;

    while(1)
    {
        count = q.pop();

        cout << count << endl;

        if( count == 2000 )
            break;

        usleep(10000);
    }
}

int main()
{
    Queue<int> q;

    thread first(bind(grabFrame,ref(q)));
    thread second(bind(showFrame,ref(q)));

    first.join();
    second.join();

    return 0;
}
