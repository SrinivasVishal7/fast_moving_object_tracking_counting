#ifndef QUEUE_H
#define QUEUE_H

#include<iostream>
#include<condition_variable>
#include<mutex>
#include<queue.h>

#include<opencv2/opencv.hpp>

using namespace std;
using namespace cv;

template <typename T>
class Queue
{
public:

    T pop()
    {
        unique_lock<mutex> mlock(mutex_);

        while (queue_.empty())
        {
            cond_.wait(mlock);
        }

        auto val = queue_.front();
        queue_.pop();
        return val;
    }

    void pop(T& item)
    {
        unique_lock<mutex> mlock(mutex_);

        while (queue_.empty())
        {
            cond_.wait(mlock);
        }

        item = queue_.front();
        queue_.pop();
    }

    void push(const T& item)
    {
        unique_lock<mutex> mlock(mutex_);
        queue_.push(item);
        mlock.unlock();
        cond_.notify_one();
    }
//    Queue()=default;
//    Queue(const Queue&) = delete;            // disable copying
//    Queue& operator=(const Queue&) = delete; // disable assignment

private:
    queue<T> queue_;
    mutex mutex_;
    condition_variable cond_;
};



#endif // QUEUE_H
