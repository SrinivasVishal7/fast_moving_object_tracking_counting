// thread example
#include <iostream>
#include <thread>

#include "queue.h"

#include<opencv2/opencv.hpp>

using namespace std;
using namespace cv;

void ConvertRGIR2RGGB(Mat BayerRGIR, Mat &BayerRGGB)
{
    //Result image after replacing the IR pixel with the G data
    BayerRGGB = BayerRGIR.clone();

    //copying the IR data and replacing the IR data with G
    for (int Row = 0; Row < BayerRGIR.rows; Row += 2)
    {
        for (int Col = 0; Col < BayerRGIR.cols; Col += 2)
        {
           //Set the IR Data with Nearby Green
           BayerRGGB.at<uchar>(Row + 1, Col) = BayerRGIR.at<uchar>(Row, Col + 1);
        }
    }
}

void grabFrame( Queue<Mat>& q )
{
    Mat currentFrame;
    Mat bgrFrame;

    VideoCapture capture = VideoCapture(1);
    capture.set(16, false);

    Mat bayer8,bayerRGGB;

    int64 start;
    double millisecondsElapsed;

    start = getTickCount();

    int frameCount = 0;

    while(1)
    {
        capture.read( currentFrame );

        if(currentFrame.empty())
        {
            printf("Error : Empty frame\n\n");
        }

        convertScaleAbs(currentFrame, bayer8, 0.249023);
        ConvertRGIR2RGGB(bayer8 , bayerRGGB);
        cvtColor(bayerRGGB, bgrFrame, COLOR_BayerRG2BGR);

        q.push( bgrFrame );

        frameCount++;

        if( frameCount % 300 == 0)
        {
            millisecondsElapsed = (getTickCount() - start) / getTickFrequency();
            cout << "Grabbing FPS : " << millisecondsElapsed << "," << ( 300 / ( millisecondsElapsed ) ) << endl;
            start = getTickCount();
        }
    }
}

void showFrame( Queue<Mat>& q )
{
    Mat current_frame;

    int64 start;
    double millisecondsElapsed;

    start = getTickCount();

    int frameCount = 0;

    while(1)
    {
        current_frame = q.pop();
        imshow("Current Frame", current_frame);
        waitKey(1);

        frameCount++;

        if( frameCount % 300 == 0)
        {
            millisecondsElapsed = (getTickCount() - start) / getTickFrequency();
            cout << "Showing FPS : " << millisecondsElapsed << "," << ( 300 / ( millisecondsElapsed ) ) << endl;
            start = getTickCount();
        }
    }
}

//int main()
//{
//    Queue<Mat> q;

//    thread first(bind(grabFrame,ref(q)));
//    thread second(bind(showFrame,ref(q)));

//    first.join();
//    second.join();

//    return 0;
//}
