TEMPLATE = app
CONFIG += console -std=c++11 -lpthread link_pkgconfig
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    testqueue.cpp

PKGCONFIG += opencv dlib-1

LIBS += -lcblas -lgfortran -lm -llapack -lblas -pthread

HEADERS += \
    queue.h
