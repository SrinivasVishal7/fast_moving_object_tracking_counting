#ifndef GLOBALS_H
#define GLOBALS_H

#define FRAME_WIDTH 672
#define FRAME_HEIGHT 380

#define DEBUG 0
#define CURRENT_FRAME_TRACKING 1
#define ALL_FRAMES 0
#define INTERACTIVE 0

#endif // GLOBALS_H
