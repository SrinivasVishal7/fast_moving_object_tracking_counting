#include "objecttracker.h"

objectTracker::objectTracker()
{
    kalmanManager = KalmanManager(FRAME_WIDTH,FRAME_HEIGHT);
}

void objectTracker::track( Queue<Mat> &bgrFramesQueueForTracking, Queue<vector<Point2d>> &currentobjectCoordinatesQueue, Queue<vector<Point>> &currentobjectDimensionsQueue, Queue<Mat> &currentTrackingFramesQueue )
{
        Mat prevFrame;
        Mat currentFrame;
        Mat currentTrackingFrame;
        Mat dlibFrame;

        vector<Point2d> currentobjectCoordinates;
        vector<Point> currentobjectDimensions;

        vector<Point2d> prevUnmatchedobjectCoordinates;
        vector<Point> prevUnmatchedobjectDimensions;

        map<int, Mat> tempKalmanPredictions;
        vector<KalmanContourMap> tempKalmanContourMapList;

        int object_count = 0;
        int frameCount = 0;

        int64 start = cv::getTickCount();
        double millisecondsElapsed = 0;

        while( 1 )
        {
            currentobjectCoordinates.clear();
            currentobjectDimensions.clear();

            currentFrame = bgrFramesQueueForTracking.pop();

            currentobjectCoordinates = currentobjectCoordinatesQueue.pop();
            currentobjectDimensions = currentobjectDimensionsQueue.pop();

            currentTrackingFrame = currentFrame.clone();
            dlibFrame = currentFrame.clone();

            // New objects found. Assign new trackers.
            if( prevUnmatchedobjectCoordinates.size() > 0 )
            {
                for (int i = 0; i < prevUnmatchedobjectCoordinates.size(); i++)
                {
                    dlib::correlation_tracker tracker;

                    int x_centre = int( prevUnmatchedobjectCoordinates[i].x );
                    int y_centre = int( prevUnmatchedobjectCoordinates[i].y );

                    int width = prevUnmatchedobjectDimensions[i].x;
                    int height = prevUnmatchedobjectDimensions[i].y;

                    dlib::cv_image<dlib::bgr_pixel> prev_frame_dlib(prevFrame);
                    tracker.start_track(prev_frame_dlib, dlib::rectangle( dlib::point(x_centre - width - 10, y_centre - height - 10), dlib::point(x_centre + width + 10, y_centre + height + 10 )));

                    dlib::cv_image<dlib::bgr_pixel> current_frame_tracking_dlib(currentFrame);
                    tracker.update(current_frame_tracking_dlib);

                    dlib::rectangle rect = tracker.get_position();

                    int dlib_x_centre = int( rect.left() + rect.right() ) / 2;
                    int dlib_y_centre = int( rect.top() + rect.bottom() ) / 2;

                    kalmanManager.createNewKalman(x_centre, y_centre + 10, dlib_x_centre, dlib_y_centre + 10);

                    object_count++;
                }

                prevUnmatchedobjectCoordinates.clear();
            }
            else
            {
                map<int, Mat> kalmanPredictions;

                kalmanManager.getTrackingKalmanPredictions(kalmanPredictions );

                tempKalmanPredictions = kalmanPredictions;

                vector<KalmanContourMap> kalmanContourMapList = kalmanManager.mapKalmansToContours(kalmanPredictions, currentobjectCoordinates);

                tempKalmanContourMapList = kalmanContourMapList;

                vector<int> contoursTracked;

                for(int i = 0; i < kalmanContourMapList.size(); i++ )
                {
                    int kalmanId = kalmanContourMapList.at(i).kalmanId;

                    int kalmanX = int( kalmanPredictions[kalmanId].at<float>(0) );
                    int kalmanY = int( kalmanPredictions[kalmanId].at<float>(1) );

                    rectangle(currentTrackingFrame, Point( kalmanX - kalmanManager.kalmanLeftMargin , kalmanY - kalmanManager.kalmanTopMargin ), Point( kalmanX + kalmanManager.kalmanRightMargin, kalmanY + kalmanManager.kalmanBottomMargin ), Scalar(0, 0, 255), 2);
                    circle(currentTrackingFrame, Point(kalmanX,kalmanY), 2, Scalar(0,255,0), -1 );
                    putText(currentTrackingFrame, to_string(kalmanId), Point( kalmanX - kalmanManager.kalmanLeftMargin - 5, kalmanY - kalmanManager.kalmanTopMargin - 5 ), CV_FONT_BLACK, .5, Scalar(0, 255, 0));

                    if( kalmanContourMapList.at(i).distances.size() <= 0 )
                    {
                        int kalmanId = kalmanContourMapList.at(i).kalmanId;
                        kalmanManager.deleteKalman( kalmanId );
                        continue;
                    }

                    int minimumDistanceIndex = distance(kalmanContourMapList.at(i).distances.begin(),min_element(kalmanContourMapList.at(i).distances.begin(),kalmanContourMapList.at(i).distances.end()));
                    int closestContourIndex = kalmanContourMapList.at(i).contourIndices[minimumDistanceIndex];

                    kalmanManager.updateKalman(kalmanContourMapList.at(i).kalmanId, currentobjectCoordinates[closestContourIndex].x, currentobjectCoordinates[closestContourIndex].y + 5);

                    if ( find(contoursTracked.begin(), contoursTracked.end(), closestContourIndex) == contoursTracked.end() )
                    {
                        contoursTracked.push_back(closestContourIndex);
                    }
                }

                sort(contoursTracked.begin(), contoursTracked.end(), [](const int a, const int b) {return a > b; });

                for( int i = 0; i < contoursTracked.size(); i++ )
                {
                    currentobjectCoordinates.erase( currentobjectCoordinates.begin() + contoursTracked.at(i) );
                    currentobjectDimensions.erase( currentobjectDimensions.begin() + contoursTracked.at(i) );
                }

                prevUnmatchedobjectCoordinates = currentobjectCoordinates;
                prevUnmatchedobjectDimensions = currentobjectDimensions;
            }

            prevFrame = currentFrame.clone();

//            putText(currentTrackingFrame, to_string(object_count), Point(50, 50), CV_FONT_BLACK, 1, Scalar(0, 0, 255));

            frameCount++;

//            currentTrackingFramesQueue.push(currentTrackingFrame);

            if( frameCount % 300 == 0)
            {
                millisecondsElapsed = (getTickCount() - start) / getTickFrequency();
                cout << "Tracking FPS : " << ( 300 / ( millisecondsElapsed ) ) << endl;
                start = getTickCount();
            }

            cout <<  "object Count : " << object_count << endl;
        }
}
