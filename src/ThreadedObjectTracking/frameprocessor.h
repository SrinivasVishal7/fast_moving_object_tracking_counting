#ifndef FRAMEPROCESSOR_H
#define FRAMEPROCESSOR_H

#include "globals.h"
#include "queue.h"

#include<opencv2/opencv.hpp>

using namespace std;
using namespace cv;

class FrameProcessor
{
    int frameWidth;
    int frameHeight;

    int leftMargin;
    int rightMargin;
    int topMargin;
    int bottomMargin;

    int lowerThreshold;
    int upperThreshold;

public:
    FrameProcessor();
    FrameProcessor(int frameWidth, int frameHeight);

    void preprocessFramesForWatershed(Queue<Mat> &grayFramesQueue, Queue<Mat> &sureBackgroundQueue, Queue<Mat> &distanceTransformMatrixQueue, Queue<double> &maxValueQueue);
    void getobjectCoordinatesUsingWatershed(Queue<Mat>& bgrFramesQueue, Queue<Mat> &sureBackgroundQueue, Queue<Mat> &distanceTransformMatrixQueue, Queue<double> &maxValueQueue, Queue<vector<Point2d> > &currentActualPositionsQueue, Queue<vector<Point> > &currentDimensionsQueue );

    void getCurrentFrameobjectCoordinates(Queue<Mat> &grabberFramesQueue, Queue<vector<Point2d> > &currentActualPositionsQueue, Queue<vector<Point> > &currentDimensionsQueue);
    void getCurrentFrameobjectCoordinatesUsingWatershedPeakLocal(Queue<Mat> &grabberFramesQueue, Queue<vector<Point2d> > &currentActualPositionsQueue, Queue<vector<Point>> &currentDimensionsQueue);
};

#endif // FRAMEPROCESSOR_H
