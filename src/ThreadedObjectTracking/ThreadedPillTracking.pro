TEMPLATE = app
CONFIG += console c++11 link_pkgconfig
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    framegrabber.cpp \
    frameprocessor.cpp \
    kalmanmanager.cpp \
    objecttracker.cpp

PKGCONFIG += opencv dlib-1

LIBS += -lcblas -lgfortran -lm -llapack -lblas

HEADERS += \
    queue.h \
    globals.h \
    framegrabber.h \
    frameprocessor.h \
    globals.h \
    kalmancontourmap.h \
    kalmanmanager.h \
    objecttracker.h
