#ifndef QUEUE_H
#define QUEUE_H

#include<iostream>
#include<mutex>
#include<condition_variable>

#include<queue.h>
#include<opencv2/opencv.hpp>

using namespace std;
using namespace cv;

//template <typename T> class Queue
//{
//    list<T>   m_queue;
//    pthread_mutex_t m_mutex;
//    pthread_cond_t  m_condv;

//public:
//    Queue()
//    {
//        pthread_mutex_init(&m_mutex, NULL);
//        pthread_cond_init(&m_condv, NULL);
//    }

//    ~Queue()
//    {
//        pthread_mutex_destroy(&m_mutex);
//        pthread_cond_destroy(&m_condv);
//    }

//    void push(T item)
//    {
//        pthread_mutex_lock(&m_mutex);
//        m_queue.push_back(item);
//        pthread_cond_signal(&m_condv);
//        pthread_mutex_unlock(&m_mutex);
//    }

//    T pop()
//    {
//        pthread_mutex_lock(&m_mutex);

//        while (m_queue.size() == 0)
//        {
//            pthread_cond_wait(&m_condv, &m_mutex);
//        }

//        T item = m_queue.front();
//        m_queue.pop_front();
//        pthread_mutex_unlock(&m_mutex);
//        return item;
//    }

//    int size()
//    {
//        pthread_mutex_lock(&m_mutex);
//        int size = m_queue.size();
//        pthread_mutex_unlock(&m_mutex);
//        return size;
//    }
//};

template <typename T>
class Queue
{
public:

    T pop()
    {
        unique_lock<mutex> mlock(mutex_);

        while (queue_.empty())
        {
            cond_.wait(mlock);
        }

        auto val = queue_.front();
        queue_.pop();
        return val;
    }

    void push(const T& item)
    {
        unique_lock<mutex> mlock(mutex_);
        queue_.push(item);
        mlock.unlock();
        cond_.notify_one();
    }

    int size()
    {
        return queue_.size();
    }

private:
    queue<T> queue_;
    mutex mutex_;
    condition_variable cond_;
};

#endif // QUEUE_H
