#include "framegrabber.h"

FrameGrabber::FrameGrabber(int mode, int streamSource, string videoSource)
{
    this->mode = mode;

    if(mode == STREAM_MODE)
    {
        capture = VideoCapture(streamSource);
        capture.set(16, false);
    }
    else
    {
        capture = VideoCapture(videoSource);
    }

    if(!capture.isOpened())
    {
        printf("Error : Capture failed\n\n");
        exit(1);
    }

    frame_width = capture.get(CAP_PROP_FRAME_WIDTH);
    frame_height = capture.get(CAP_PROP_FRAME_HEIGHT);
}

void FrameGrabber::ConvertRGIR2RGGB(Mat BayerRGIR, Mat &BayerRGGB)
{
    //Result image after replacing the IR pixel with the G data
    BayerRGGB = BayerRGIR.clone();

    //copying the IR data and replacing the IR data with G
    for (int Row = 0; Row < BayerRGIR.rows; Row += 2)
    {
        for (int Col = 0; Col < BayerRGIR.cols; Col += 2)
        {
           //Set the IR Data with Nearby Green
           BayerRGGB.at<uchar>(Row + 1, Col) = BayerRGIR.at<uchar>(Row, Col + 1);
        }
    }
}

void FrameGrabber::grabFrame(Queue<Mat> &grayFramesQueue, Queue<Mat> &bgrFramesQueue, Queue<Mat> &bgrFramesQueueForTracker)
{
    Mat currentFrame;
    Mat bgrFrame;
    Mat grayFrame;

    int frameCount = 0;

    int64 start;
    double millisecondsElapsed;

    start = getTickCount();

//    stringstream ss;

    while(1)
    {
        capture.read( currentFrame );

        if(currentFrame.empty())
        {
            printf("Error : Empty frame\n\n");
//            cout << "Grabber Thread " << bgrFramesQueue.size() << endl;
//            cout << "BGR Queue for tracking " << bgrFramesQueueForTracker.size() << endl;
            break;
        }

        if(mode == STREAM_MODE)
        {
            Mat bayer8,bayerRGGB;

            convertScaleAbs(currentFrame, bayer8, 0.6);
            ConvertRGIR2RGGB(bayer8 , bayerRGGB);
            cvtColor(bayerRGGB, bgrFrame, COLOR_BayerRG2BGR);
            cvtColor(bgrFrame, grayFrame, COLOR_BGR2GRAY);

            bgrFramesQueue.push(bgrFrame.clone());
            grayFramesQueue.push(grayFrame.clone());
            bgrFramesQueueForTracker.push(bgrFrame.clone());

            frameCount++;

            if( frameCount % 300 == 0)
            {
                millisecondsElapsed = (getTickCount() - start) / getTickFrequency();
                cout << "Grabbing FPS : " << millisecondsElapsed << "," << ( 300 / ( millisecondsElapsed ) ) << endl;
                start = getTickCount();
            }
        }
        else
        {
            frameCount++;

//            cvtColor(currentFrame, grayFrame, COLOR_BGR2GRAY);
            bgrFramesQueue.push(currentFrame.clone());
//            grayFramesQueue.push(grayFrame.clone());

//            ss << "/home/prabu-test/Vikas/repo/CPP/objectdetection/Images/image_"  << frameCount << ".png";

//            imwrite( ss.str(), bgrFramesQueue.pop() );

//            ss.str("");

//            imshow("Grabbed Frame",currentFrame);
//            waitKey(1);

//            bgrFramesQueueForTracker.push(currentFrame.clone());
        }
    }
}
