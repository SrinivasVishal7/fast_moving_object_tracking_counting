#include <iostream>
#include <dlib/opencv.h>
#include <dlib/image_processing.h>
#include <dlib/gui_widgets.h>
#include <dlib/image_io.h>
#include <dlib/dir_nav.h>
#include <ctime>
#include <algorithm>

#include "framegrabber.h"
#include "frameprocessor.h"
#include "kalmanmanager.h"
#include "objecttracker.h"

using namespace cv;
using namespace std;

int main()
{
    namedWindow("Current Tracking Frame", WINDOW_AUTOSIZE);

    FrameGrabber grabber(STREAM_MODE, 0, "/home/prabu-test/Vikas/repo/CPP/objectdetection/Videos/emulator_5_1.avi");
    FrameProcessor frameProcessor(FRAME_WIDTH,FRAME_HEIGHT);
    objectTracker objectTracker = objectTracker();

    // Layer 1
    Queue<Mat> grayFramesQueue;
    Queue<Mat> bgrFramesQueue;
    Queue<Mat> bgrFramesQueueForTracking;

    // Layer 2
    Queue<Mat> sureBackgroundQueue;
    Queue<Mat> distanceTransformMatrixQueue;
    Queue<double> maxValueQueue;

    // Layer 3
    Queue<vector<Point2d>> currentobjectCoordinatesQueue;
    Queue<vector<Point>> currentobjectDimensionsQueue;

    // Layer 3
    Queue<Mat> currentTrackingFramesQueue;

    // Threads
    thread grabberThread(bind( &FrameGrabber::grabFrame, grabber, ref(grayFramesQueue), ref(bgrFramesQueue), ref(bgrFramesQueueForTracking)));

//    thread processorThread(bind( &FrameProcessor::getCurrentFrameobjectCoordinates, frameProcessor, ref(bgrFramesQueue), ref(bgrFramesQueueForTracking), ref(currentobjectCoordinatesQueue), ref(currentobjectDimensionsQueue)));

//    thread processorThread(bind( &FrameProcessor::preprocessFramesForWatershed, frameProcessor, ref(grayFramesQueue), ref(sureBackgroundQueue), ref(distanceTransformMatrixQueue), ref(maxValueQueue)));
//    thread watershedThread(bind( &FrameProcessor::getobjectCoordinatesUsingWatershed, frameProcessor, ref(bgrFramesQueue) , ref(sureBackgroundQueue), ref(distanceTransformMatrixQueue), ref(maxValueQueue), ref(currentobjectCoordinatesQueue), ref(currentobjectDimensionsQueue)));

      thread watershedThread(bind( &FrameProcessor::getCurrentFrameobjectCoordinatesUsingWatershedPeakLocal, frameProcessor, ref(bgrFramesQueue), ref(currentobjectCoordinatesQueue), ref(currentobjectDimensionsQueue)));

    thread trackerThread(bind( &objectTracker::track, objectTracker, ref(bgrFramesQueueForTracking), ref(currentobjectCoordinatesQueue), ref(currentobjectDimensionsQueue), ref(currentTrackingFramesQueue)));

//    stringstream ss;

//    int frameCount = 0;

//    while(1)
//    {
//        Mat currentTrackingFrame = currentTrackingFramesQueue.pop();
//        imshow("Current Tracking Frame", currentTrackingFrame);

//        ss << "/home/prabu-test/Vikas/repo/CPP/objectdetection/Images/image_" << frameCount << ".png";

//        imwrite(ss.str(), currentTrackingFrame);

//        ss.str("");

//        frameCount++;
//    }

    grabberThread.join();
//    processorThread.join();
    watershedThread.join();
    trackerThread.join();
}
