#include "frameprocessor.h"

FrameProcessor::FrameProcessor()
{

}

FrameProcessor::FrameProcessor(int frame_width, int frame_height)
{
    this->frameWidth = frame_width;
    this->frameHeight = frame_height;

    this->leftMargin = 30;
    this->rightMargin = 30;
    this->topMargin = 30;
    this->bottomMargin = 40;

    this->lowerThreshold = 80;
    this->upperThreshold = 255;
}

void FrameProcessor::preprocessFramesForWatershed(Queue<Mat> &grayFramesQueue, Queue<Mat> &sureBackgroundQueue, Queue<Mat> &distanceTransformMatrixQueue, Queue<double>& maxValueQueue)
{
    Mat grayFrame;
    Mat thresholdFrame;
    Mat sureBackground;
    Mat dilatedFrame;
    Mat distanceTransformMatrix;
    Mat morphological_kernel;

    double minVal, maxVal;

    int frameCount = 0;

    int64 start;
    double millisecondsElapsed;

    start = getTickCount();

    morphological_kernel = Mat::ones(5, 5, CV_8UC1);

    while(1)
    {
        grayFrame = grayFramesQueue.pop();

        threshold(grayFrame, thresholdFrame, lowerThreshold, upperThreshold, CV_THRESH_BINARY);

        dilate(thresholdFrame, dilatedFrame, morphological_kernel, Point(-1,-1), 2);

        sureBackground = dilatedFrame.clone();

        distanceTransform(thresholdFrame, distanceTransformMatrix, CV_DIST_L2, 5);

        minMaxIdx(distanceTransformMatrix, &minVal, &maxVal);

        sureBackgroundQueue.push(sureBackground.clone());
        distanceTransformMatrixQueue.push(distanceTransformMatrix.clone());
        maxValueQueue.push(maxVal);

        frameCount++;

        if( frameCount % 300 == 0)
        {
            millisecondsElapsed = (getTickCount() - start) / getTickFrequency();
            cout << "Preprocess FPS : " << millisecondsElapsed << "," << ( 300 / ( millisecondsElapsed ) ) << endl;
            start = getTickCount();
        }
    }
}

void FrameProcessor::getobjectCoordinatesUsingWatershed(Queue<Mat>& bgrFramesQueue, Queue<Mat> &sureBackgroundQueue, Queue<Mat> &distanceTransformMatrixQueue, Queue<double> &maxValueQueue, Queue<vector<Point2d> > &currentActualPositionsQueue, Queue<vector<Point> > &currentDimensionsQueue)
{
    Mat unknownArea;
    Mat markers;
    Mat zeroMatrix;
    Mat sureForeground;
    Mat mark;
    vector< vector<cv::Point> > contours;
    Mat inputFrame;
    double maxVal;
    Mat distanceTransformMatrix;
    Mat sureBackground;

    int frameCount = 0;

    int64 start;
    double millisecondsElapsed;

    start = getTickCount();

    while(1)
    {
        distanceTransformMatrix = distanceTransformMatrixQueue.pop();
        sureBackground = sureBackgroundQueue.pop();
        maxVal = maxValueQueue.pop();
        inputFrame = bgrFramesQueue.pop();

        threshold(distanceTransformMatrix, sureForeground, .999 * maxVal, upperThreshold, CV_THRESH_BINARY);

        sureForeground.convertTo(sureForeground, CV_8U);

        subtract(sureBackground, sureForeground, unknownArea);

        connectedComponents(sureForeground, markers);

        markers = markers + 1;

        zeroMatrix = Mat::zeros(markers.size(), markers.type());
        bitwise_and(markers, zeroMatrix, markers, unknownArea);

        watershed(inputFrame, markers);

        mark = Mat::zeros(markers.size(), CV_8UC1);
        markers.convertTo(mark, CV_8UC1);
        bitwise_not(mark, mark);

        findContours(sureForeground.clone(), contours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE);

        Point2f center;
        float radius;

        vector<Point2d> currentActualPositions;
        vector<Point> currentDimensions;

        for (int i = 2; i <= contours.size() + 1; i++)
        {
            Mat mask = Mat::zeros(markers.size(), CV_8UC1);

            mask = (markers == i);

            vector<vector<Point>> maskContours;
            findContours(mask.clone(), maskContours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE);

            if (maskContours.size() <= 0)
            {
                continue;
            }

            minEnclosingCircle(maskContours[0], center, radius );

            if( ( center.x + radius + rightMargin <= frameWidth ) && ( center.y + radius + bottomMargin <= frameHeight ) && ( center.x - radius - leftMargin <= frameWidth ) && ( center.y - radius - topMargin >=0 ) )
            {
                currentActualPositions.push_back(Point2d(center.x, center.y));
                currentDimensions.push_back(Point(radius,radius));
            }
        }

        currentActualPositionsQueue.push(currentActualPositions);
        currentDimensionsQueue.push(currentDimensions);

        frameCount++;

        if( frameCount % 300 == 0)
        {
            millisecondsElapsed = (getTickCount() - start) / getTickFrequency();
            cout << "Watershed FPS : " << millisecondsElapsed << "," << ( 300 / ( millisecondsElapsed ) ) << endl;
            start = getTickCount();
        }
    }
}

void FrameProcessor::getCurrentFrameobjectCoordinates(Queue<Mat> &grabberFramesQueue, Queue<vector<Point2d>> &currentActualPositionsQueue, Queue<vector<Point>> &currentDimensionsQueue)
{
   // OpenCV preprocessing
   Mat grayFrame, thresholdedFrame;
   Mat erodedFrame1, dilatedFrame;
   Mat kernel = Mat::eye(Size(5,5), CV_8U);
   Mat inputFrame;
   vector<vector<Point>>contours;

   int frameCount = 0;

   int64 start;
   double millisecondsElapsed;

   start = getTickCount();

   while(1)
   {
       vector<Point2d> currentActualPositions;
       vector<Point> currentDimensions;

       inputFrame = grabberFramesQueue.pop();

       cvtColor( inputFrame, grayFrame, COLOR_BGR2GRAY );

       threshold( grayFrame, thresholdedFrame, lowerThreshold, upperThreshold, 0 );

       erode( thresholdedFrame, erodedFrame1, kernel, Point(-1,-1), 1 );
       dilate( erodedFrame1, dilatedFrame, kernel, Point(-1,-1), 1 );
    //   erode( dilatedFrame, erodedFrame2 , kernel, Point(-1,-1), 1 );

       // finding all contours in current frame
       findContours( dilatedFrame.clone(), contours, RETR_EXTERNAL, CHAIN_APPROX_SIMPLE);

       // append all contous to a list
       for (int i=0; i < contours.size(); i++)
       {
           Rect contourRect = boundingRect(contours[i]);

           // calculate the centerpoint
           double centerX = contourRect.x + 0.5 * contourRect.width;
           double centerY = contourRect.y + 0.5 * contourRect.height;

           if ( ( centerX + contourRect.width + rightMargin <= frameWidth ) &&
                ( centerY + contourRect.height + bottomMargin <= frameHeight ) &&
                ( centerX - contourRect.width - leftMargin >= 0 ) &&
                ( centerY - contourRect.height - topMargin >= 0 ) )
           {
               // Actual centres of all the objects in the current frame
               currentActualPositions.push_back(Point2d(centerX, centerY));
               currentDimensions.push_back(Point(contourRect.width,contourRect.height));
           }
       }

       currentActualPositionsQueue.push(currentActualPositions);
       currentDimensionsQueue.push(currentDimensions);

       frameCount++;

       if( frameCount % 300 == 0)
       {
           millisecondsElapsed = (getTickCount() - start) / getTickFrequency();
           cout << "object Coordinates FPS : " << millisecondsElapsed << "," << ( 300 / ( millisecondsElapsed ) ) << endl;
           start = getTickCount();
       }
   }
}

void FrameProcessor:: getCurrentFrameobjectCoordinatesUsingWatershedPeakLocal(Queue<Mat> &grabberFramesQueue,  Queue<vector<Point2d>> &currentActualPositionsQueue, Queue<vector<Point>> &currentDimensionsQueue)
{
    Mat grayFrame;
    Mat thresholdFrame;
    Mat sureBackground, sureForeground;
    Mat distanceTransformMatrix;
    Mat unknownArea;
    Mat markers;
    Mat zeroMatrix;
    Mat morphologicalKernel;
    Mat dilateKernel;
    Mat peakLocalFrame;
    Mat inputFrame;
    Mat drawnContoursFrame;
    vector<vector<Point>> originalContours;

    int minDistance = 11;

    vector< vector<cv::Point> > contours;

    while(1)
    {
        inputFrame = grabberFramesQueue.pop();

        // Create binary image from source image
        cvtColor(inputFrame, grayFrame, CV_BGR2GRAY);
        threshold(grayFrame, thresholdFrame, lowerThreshold, upperThreshold, CV_THRESH_BINARY);

        morphologicalKernel = Mat::ones(5, 5, CV_8UC1);

        // Dilate a bit the threshold image
        dilate(thresholdFrame, sureBackground, morphologicalKernel, Point(-1,-1), 2);

        findContours(sureBackground.clone(),originalContours,RETR_EXTERNAL,CHAIN_APPROX_SIMPLE);

        if( originalContours.size() > 0 )
        {
            drawnContoursFrame = Mat::zeros(sureBackground.size(), sureBackground.type());
            drawContours(drawnContoursFrame, originalContours, -1, Scalar::all(255), -1);
            sureBackground = drawnContoursFrame.clone();
        }

        // Perform the distance transform algorithm
        distanceTransform(sureBackground, distanceTransformMatrix, CV_DIST_L2, 5);
        //writeCSV("dst_opencv.csv", dist_transform_frame);

        //Finding the peak local maximum as in scipy
        peakLocalFrame = distanceTransformMatrix.clone();
        dilateKernel = Mat::ones(minDistance, minDistance, CV_8UC1);
        dilate(peakLocalFrame, peakLocalFrame, dilateKernel, Point(-1,-1), 1);
        peakLocalFrame = (peakLocalFrame == distanceTransformMatrix);

        sureForeground = Mat::zeros(peakLocalFrame.size(), peakLocalFrame.type());
        bitwise_and(peakLocalFrame, peakLocalFrame, sureForeground, sureBackground);


        // Normalize the distance image for range = {0.0, 1.0}
        // so we can visualize and threshold it
        // normalize(dist_transform_frame, dist_transform_frame, 0, 1., cv::NORM_MINMAX);
        //cv::minMaxIdx(dist_transform_frame, &minVal, &maxVal);

        // Threshold to obtain the peaks
        // This will be the markers for the foreground objects
        //threshold(dist_transform_frame, sure_fg, .99*maxVal, 255, CV_THRESH_BINARY);

        sureForeground.convertTo(sureForeground, CV_8U);
        findContours(sureForeground.clone(), contours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE);

        if(contours.size() > 1)
        {
            imshow("sureForeGround", sureForeground);
            imshow("sureBackground", sureBackground);
            waitKey(0);
        }

        subtract(sureBackground, sureForeground, unknownArea);
        connectedComponents(sureForeground, markers);
        markers = markers + 1;

        zeroMatrix = Mat::zeros(markers.size(), markers.type());
        bitwise_and(markers, zeroMatrix, markers, unknownArea);

        // Perform the watershed algorithm
        watershed(inputFrame, markers);

        // Create the result image
        Point2f center;
        float radius;

        vector<Point2d> currentActualPositions;
        vector<Point> currentDimensions;

        // marker = 1 is the background
        // Drawing circles except for the background
        for (int i = 2; i <= contours.size() + 1; i++)
        {
            Mat mask = Mat::zeros(markers.size(), CV_8UC1);

            mask = (markers == i);
            vector<vector<Point>> maskContours;
            findContours(mask.clone(), maskContours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE);

            if (maskContours.size() <= 0)
            {
                continue;
            }

            minEnclosingCircle(maskContours[0], center, radius );

            if( ( center.x + radius + rightMargin <= frameWidth ) && ( center.y + radius + bottomMargin <= frameHeight ) && ( center.x - radius - leftMargin <= frameWidth ) && ( center.y - radius - topMargin >=0 ) )
            {
                currentActualPositions.push_back(Point2d(center.x, center.y));
                currentDimensions.push_back(Point(radius,radius));
            }
        }

        currentActualPositionsQueue.push(currentActualPositions);
        currentDimensionsQueue.push(currentDimensions);
    }
}
