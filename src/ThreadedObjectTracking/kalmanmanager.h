#ifndef KALMANMANAGER_H
#define KALMANMANAGER_H

#include<opencv2/opencv.hpp>
#include "kalmancontourmap.h"

using namespace std;
using namespace cv;

class KalmanManager
{
    map<int, KalmanFilter> kalmanFiltersMap;

    Mat initial_measurement_matrix;
    Mat initial_transition_matrix;
    Mat initial_process_noise_conversion;
    Mat initial_measurement_noise_conversion;

    int frameWidth;
    int frameHeight;

    static int kalmanId;

public:

    int kalmanTopMargin;
    int kalmanBottomMargin;
    int kalmanLeftMargin;
    int kalmanRightMargin;

    KalmanManager();
    KalmanManager(int frameWidth, int frameHeight);

    void createNewKalman(int prev_x, int prev_y, int current_x, int current_y);
    void updateKalman(int kalmanId, int current_x, int current_y);
    void deleteKalman(int kalmanId);
    void getTrackingKalmanPredictions(map<int, Mat> &kalmanPredictions);
    vector<KalmanContourMap> mapKalmansToContours(map<int, Mat> kalmanPredictions, vector<Point2d> contourPositions );
};

#endif // KALMANMANAGER_H
