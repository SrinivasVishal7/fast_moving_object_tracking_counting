#ifndef objectTRACKER_H
#define objectTRACKER_H

#include <dlib/opencv.h>
#include <dlib/image_processing.h>
#include <dlib/gui_widgets.h>
#include <dlib/image_io.h>
#include <dlib/dir_nav.h>
#include <ctime>
#include <algorithm>

#include "globals.h"
#include "queue.h"
#include "kalmanmanager.h"

class objectTracker
{
    KalmanManager kalmanManager;

public:
    objectTracker();

    void track(Queue<Mat> &bgrFramesQueueForTracking, Queue<vector<Point2d>> &currentobjectCoordinatesQueue, Queue<vector<Point>> &currentobjectDimensionsQueue, Queue<Mat> &currentTrackingFramesQueue);
};

#endif // objectTRACKER_H
