#ifndef FRAMEGRABBER_H
#define FRAMEGRABBER_H

#include<opencv2/opencv.hpp>
#include <frameprocessor.h>

using namespace std;
using namespace cv;

#define STREAM_MODE 0
#define VIDEO_MODE 1

class FrameGrabber
{
    VideoCapture capture;
    int frame_width;
    int frame_height;
    int mode;

public:
    FrameGrabber(int mode, int stream_source, string video_source);
    void ConvertRGIR2RGGB(Mat BayerRGIR, Mat &BayerRGGB);
    Mat grabFrame();
};

#endif // FRAMEGRABBER_H
