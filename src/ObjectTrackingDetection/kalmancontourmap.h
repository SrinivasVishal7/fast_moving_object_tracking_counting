#ifndef KALMANCONTOURMAP_H
#define KALMANCONTOURMAP_H

#include<opencv2/opencv.hpp>
#include<iostream>

using namespace std;
using namespace cv;

class KalmanContourMap
{
public:
    KalmanContourMap() {}

    int kalmanId;
    vector<int> contourIndices;
    vector<int> distances;
};

class object
{

public:

    object() { this->largestArea = 0; }

    vector<Point> largestContour;
    int largestArea;
};

#endif // KALMANCONTOURMAP_H
