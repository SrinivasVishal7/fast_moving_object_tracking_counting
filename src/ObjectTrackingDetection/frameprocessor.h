#ifndef FRAMEPROCESSOR_H
#define FRAMEPROCESSOR_H

#include "globals.h"
#include<opencv2/opencv.hpp>

using namespace std;
using namespace cv;

class FrameProcessor
{
    int frameWidth;
    int frameHeight;

    int leftMargin;
    int rightMargin;
    int topMargin;
    int bottomMargin;

    int lowerThreshold;
    int upperThreshold;

public:
    FrameProcessor();
    FrameProcessor(int frameWidth, int frameHeight);
    void getCurrentFrameobjectCoordinatesUsingWatershed(Mat input_frame, Mat &output_frame, vector<Point2d> &current_actual_positions, vector<Point> &current_dimensions);
    void getCurrentFrameobjectCoordinates(Mat input_frame, Mat &output_frame, vector<Point2d> &current_actual_positions, vector<Point> &current_dimensions, vector<vector<Point> > &currentobjectContours);
    void getCurrentFrameobjectCoordinatesUsingWatershedPeakLocal(Mat input_frame, Mat &output_frame, vector<Point2d> &current_actual_positions, vector<Point> &current_dimensions);
};

#endif // FRAMEPROCESSOR_H
