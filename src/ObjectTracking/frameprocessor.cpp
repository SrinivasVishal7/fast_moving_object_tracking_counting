#include "frameprocessor.h"

FrameProcessor::FrameProcessor()
{

}

FrameProcessor::FrameProcessor(int frame_width, int frame_height)
{
    this->frameWidth = frame_width;
    this->frameHeight = frame_height;

    this->leftMargin = 30;
    this->rightMargin = 30;
    this->topMargin = 30;
    this->bottomMargin = 40;

    this->lowerThreshold = 150;
    this->upperThreshold = 255;
}

void FrameProcessor::getCurrentFrameobjectCoordinatesUsingWatershed(Mat inputFrame, Mat &outputFrame, vector<Point2d> &currentActualPositions, vector<Point> &currentDimensions)
{
    Mat grayFrame;
    Mat thresholdFrame;
    Mat sureBackground, sureForeground;
    Mat dilatedFrame;
    Mat distanceTransformMatrix;
    Mat unknownArea;
    Mat markers;
    Mat mask;
    Mat zeroMatrix;
    Mat morphological_kernel;
    Mat mark;
    vector< vector<cv::Point> > contours;

    double minVal, maxVal;

    cvtColor(inputFrame, grayFrame, CV_BGR2GRAY);
    threshold(grayFrame, thresholdFrame, lowerThreshold, upperThreshold, CV_THRESH_BINARY);

    morphological_kernel = Mat::ones(5, 5, CV_8UC1);
    dilate(thresholdFrame, dilatedFrame, morphological_kernel, Point(-1,-1), 2);

    sureBackground = dilatedFrame.clone();

    distanceTransform(thresholdFrame, distanceTransformMatrix, CV_DIST_L2, 5);

    minMaxIdx(distanceTransformMatrix, &minVal, &maxVal);
    threshold(distanceTransformMatrix, sureForeground, .999 * maxVal, upperThreshold, CV_THRESH_BINARY);

    sureForeground.convertTo(sureForeground, CV_8U);

    subtract(sureBackground, sureForeground, unknownArea);

    connectedComponents(sureForeground, markers);

    markers = markers + 1;

    mask = inputFrame.clone();

    zeroMatrix = Mat::zeros(markers.size(), markers.type());
    bitwise_and(markers, zeroMatrix, markers, unknownArea);

    watershed(inputFrame, markers);

    mark = Mat::zeros(markers.size(), CV_8UC1);
    markers.convertTo(mark, CV_8UC1);
    bitwise_not(mark, mark);

    findContours(sureForeground.clone(), contours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE);

    #if ALL_FRAMES
        imshow("Foreground", sureForeground);
    #endif

    Point2f center;
    float radius;

    #if ALL_FRAMES
        Mat contourFrame = inputFrame.clone();
        Scalar s = Scalar(0,0,255);
    #endif

    for (int i = 2; i <= contours.size() + 1; i++)
    {
        Mat mask = Mat::zeros(markers.size(), CV_8UC1);

        mask = (markers == i);

        vector<vector<Point>> maskContours;
        findContours(mask.clone(), maskContours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE);

        if (maskContours.size() <= 0)
        {
            continue;
        }

        minEnclosingCircle(maskContours[0], center, radius );

        #if ALL_FRAMES
            circle( contourFrame, center, (int)radius, s, 2, 8, 0 );
            imshow("Contour Frame", contourFrame);
        #endif

        if( ( center.x + radius + rightMargin <= frameWidth ) && ( center.y + radius + bottomMargin <= frameHeight ) && ( center.x - radius - leftMargin <= frameWidth ) && ( center.y - radius - topMargin >=0 ) )
        {
            currentActualPositions.push_back(Point2d(center.x, center.y));
            currentDimensions.push_back(Point(radius,radius));
        }
    }

    outputFrame = thresholdFrame.clone();
}

void FrameProcessor::getCurrentFrameobjectCoordinates(Mat input_frame, Mat& outputFrame, vector<Point2d> &currentActualPositions, vector<Point> &currentDimensions)
{
   // OpenCV preprocessing
   Mat grayFrame, thresholdedFrame;
   Mat erodedFrame1, erodedFrame2, dilatedFrame;
   Mat kernel = Mat::eye(Size(5,5), CV_8U);

   vector<vector<Point>>contours;

   cvtColor( input_frame, grayFrame, COLOR_BGR2GRAY );

   threshold( grayFrame, thresholdedFrame, lowerThreshold, upperThreshold, 0 );

   erode( thresholdedFrame, erodedFrame1, kernel, Point(-1,-1), 1 );
   dilate( erodedFrame1, dilatedFrame, kernel, Point(-1,-1), 1 );
//   erode( dilatedFrame, erodedFrame2 , kernel, Point(-1,-1), 1 );

   // finding all contours in current frame
   findContours( dilatedFrame.clone(), contours, RETR_EXTERNAL, CHAIN_APPROX_SIMPLE);

   // append all contous to a list
   for (int i=0; i < contours.size(); i++)
   {
       Rect contourRect = boundingRect(contours[i]);

       // calculate the centerpoint
       double centerX = contourRect.x + 0.5 * contourRect.width;
       double centerY = contourRect.y + 0.5 * contourRect.height;

       if ( ( centerX + contourRect.width + rightMargin <= frameWidth ) &&
            ( centerY + contourRect.height + bottomMargin <= frameHeight ) &&
            ( centerX - contourRect.width - leftMargin >= 0 ) &&
            ( centerY - contourRect.height - topMargin >= 0 ) )
       {
           // Actual centres of all the objects in the current frame
           currentActualPositions.push_back(Point2d(centerX, centerY));
           currentDimensions.push_back(Point(contourRect.width,contourRect.height));
       }
   }

   outputFrame = dilatedFrame.clone();
}

void FrameProcessor:: getCurrentFrameobjectCoordinatesUsingWatershedPeakLocal(Mat inputFrame, Mat &outputFrame, vector<Point2d> &currentActualPositions, vector<Point> &currentDimensions)
{
    Mat grayFrame;
    Mat thresholdFrame;
    Mat sureBackground, sureForeground;
    Mat distanceTransformMatrix;
    Mat unknownArea;
    Mat markers;
    Mat zeroMatrix;
    Mat morphologicalKernel;
    Mat dilateKernel;
    Mat peakLocalFrame;

    int minDistance = 11;

    vector< vector<cv::Point> > contours;

    // Create binary image from source image
    cvtColor(inputFrame, grayFrame, CV_BGR2GRAY);
    threshold(grayFrame, thresholdFrame, lowerThreshold, upperThreshold, CV_THRESH_BINARY);

    morphologicalKernel = Mat::ones(5, 5, CV_8UC1);

    // Dilate a bit the threshold image
    dilate(thresholdFrame, sureBackground, morphologicalKernel, Point(-1,-1), 2);

    // Perform the distance transform algorithm
    distanceTransform(sureBackground, distanceTransformMatrix, CV_DIST_L2, 5);
    //writeCSV("dst_opencv.csv", dist_transform_frame);q

    //Finding the peak local maximum as in scipy
    peakLocalFrame = distanceTransformMatrix.clone();
    dilateKernel = Mat::ones(minDistance, minDistance, CV_8UC1);
    dilate(peakLocalFrame, peakLocalFrame, dilateKernel, Point(-1,-1), 1);
    peakLocalFrame = (peakLocalFrame == distanceTransformMatrix);

    sureForeground = Mat::zeros(peakLocalFrame.size(), peakLocalFrame.type());
    bitwise_and(peakLocalFrame, peakLocalFrame, sureForeground, sureBackground);


    // Normalize the distance image for range = {0.0, 1.0}
    // so we can visualize and threshold it
    // normalize(dist_transform_frame, dist_transform_frame, 0, 1., cv::NORM_MINMAX);
    //cv::minMaxIdx(dist_transform_frame, &minVal, &maxVal);

    // Threshold to obtain the peaks
    // This will be the markers for the foreground objects
    //threshold(dist_transform_frame, sure_fg, .99*maxVal, 255, CV_THRESH_BINARY);

    sureForeground.convertTo(sureForeground, CV_8U);
    findContours(sureForeground.clone(), contours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE);

    subtract(sureBackground, sureForeground, unknownArea);
    connectedComponents(sureForeground, markers);
    markers = markers + 1;

    zeroMatrix = Mat::zeros(markers.size(), markers.type());
    bitwise_and(markers, zeroMatrix, markers, unknownArea);

    // Perform the watershed algorithm
    watershed(inputFrame, markers);

    #if ALL_FRAMES
        putText(sureForeground, to_string(contours.size()), Point(50, 50), CV_FONT_BLACK, 1, Scalar(0, 0, 255));
        cout << "Contour Size " << contours.size() << endl;
        imshow("Sure Foreground", sureForeground);
    #endif

    // Create the result image
    Mat contourFrame = Mat::zeros(markers.size(), CV_8UC3);
    Point2f center;
    float radius;

    // marker = 1 is the background
    // Drawing circles except for the background
    for (int i = 2; i <= contours.size() + 1; i++)
    {
        Mat mask = Mat::zeros(markers.size(), CV_8UC1);

        mask = (markers == i);
        vector<vector<Point>> maskContours;
        findContours(mask.clone(), maskContours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE);

        if (maskContours.size() <= 0)
        {
            continue;
        }

        minEnclosingCircle(maskContours[0], center, radius );

    #if ALL_FRAMES
        circle( contourFrame, center, (int)radius, (0,0,255), 2, 8, 0 );
        imshow("Contour Frame", contourFrame);
    #endif

        if( ( center.x + radius + rightMargin <= frameWidth ) && ( center.y + radius + bottomMargin <= frameHeight ) && ( center.x - radius - leftMargin <= frameWidth ) && ( center.y - radius - topMargin >=0 ) )
        {
            currentActualPositions.push_back(Point2d(center.x, center.y));
            currentDimensions.push_back(Point(radius,radius));
        }
    }

    imshow("contourFrame",contourFrame);
//    imshow("markers",markers*10000);

    outputFrame = thresholdFrame.clone();
}
