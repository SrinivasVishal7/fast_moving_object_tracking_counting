#ifndef KALMANCONTOURMAP_H
#define KALMANCONTOURMAP_H

#include<opencv2/opencv.hpp>
#include<iostream>

using namespace std;

class KalmanContourMap
{
public:
    KalmanContourMap() {}

    int kalmanId;
    vector<int> contourIndices;
    vector<int> distances;
};

#endif // KALMANCONTOURMAP_H
