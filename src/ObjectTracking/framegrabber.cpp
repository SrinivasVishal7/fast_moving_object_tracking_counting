#include "framegrabber.h"

FrameGrabber::FrameGrabber(int mode, int streamSource, string videoSource)
{
    this->mode = mode;

    if(mode == STREAM_MODE)
    {
        capture = VideoCapture(streamSource);
        capture.set(16, false);
    }
    else
    {
        capture = VideoCapture(videoSource);
    }

    if(!capture.isOpened())
    {
        printf("Error : Capture failed\n\n");
        exit(1);
    }

    frame_width = capture.get(CAP_PROP_FRAME_WIDTH);
    frame_height = capture.get(CAP_PROP_FRAME_HEIGHT);
}

void FrameGrabber::ConvertRGIR2RGGB(Mat BayerRGIR, Mat &BayerRGGB)
{
    //Result image after replacing the IR pixel with the G data
    BayerRGGB = BayerRGIR.clone();

    //copying the IR data and replacing the IR data with G
    for (int Row = 0; Row < BayerRGIR.rows; Row += 2)
    {
        for (int Col = 0; Col < BayerRGIR.cols; Col += 2)
        {
           //Set the IR Data with Nearby Green
           BayerRGGB.at<uchar>(Row + 1, Col) = BayerRGIR.at<uchar>(Row, Col + 1);
        }
    }
}

Mat FrameGrabber::grabFrame()
{
    Mat currentFrame;
    Mat bgrFrame;

    capture.read( currentFrame );

    if(currentFrame.empty())
    {
        printf("Error : Empty frame\n\n");
    }

    if(mode == STREAM_MODE)
    {
        Mat bayer8,bayerRGGB;

        convertScaleAbs(currentFrame, bayer8, 0.6);
        ConvertRGIR2RGGB(bayer8 , bayerRGGB);
        cvtColor(bayerRGGB, bgrFrame, COLOR_BayerRG2BGR);

        return bgrFrame;
    }

    return currentFrame;
}
