TEMPLATE = app
CONFIG += console c++11 link_pkgconfig
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    frameprocessor.cpp \
    framegrabber.cpp \
    kalmanmanager.cpp

HEADERS += \
    frameprocessor.h \
    framegrabber.h \
    kalmanmanager.h \
    kalmancontourmap.h \
    globals.h

PKGCONFIG += opencv dlib-1

LIBS += -lcblas -lgfortran -lm -llapack -lblas
