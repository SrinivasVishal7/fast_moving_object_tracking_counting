#include "kalmanmanager.h"

int KalmanManager::kalmanId=1;

KalmanManager::KalmanManager()
{
}

KalmanManager::KalmanManager(int frameWidth, int frameHeight)
{
    this->frameWidth = frameWidth;
    this->frameHeight = frameHeight;

    kalman_top_margin = 30;
    kalman_bottom_margin = 40;
    kalman_left_margin = 30;
    kalman_right_margin = 30;

    initial_measurement_matrix = (Mat_<float>(2, 4) << 1,0, 0,0, 0,1, 0,0 );
    initial_transition_matrix = (Mat_<float>(4, 4) << 1,0,1,0,   0,1,0,1,  0,0,1,0,  0,0,0,1);
    initial_process_noise_conversion = (Mat_<float>(4, 4) << 1,0,0,0,   0,1,0,0,  0,0,1,0,  0,0,0,1) * 0.03;
    initial_measurement_noise_conversion = (Mat_<float>(2, 2) << 1,0,   0,1) * 0.00003;
}

void KalmanManager::createNewKalman(int prev_x, int prev_y, int current_x, int current_y)
{
    KalmanFilter kalman = KalmanFilter(4,2);
    kalman.measurementMatrix = initial_measurement_matrix;
    kalman.transitionMatrix = initial_transition_matrix;
    kalman.processNoiseCov = initial_process_noise_conversion;
    kalman.measurementNoiseCov = initial_measurement_noise_conversion;

    setIdentity(kalman.errorCovPost, Scalar::all(.1));

    Mat_<float> measurement(2,1);
    measurement.setTo(Scalar(0));

    measurement(0) = prev_x;
    measurement(1) = prev_y;

    kalman.correct( measurement );
    kalman.predict();

    measurement(0) = current_x;
    measurement(1) = current_y;

    kalman.correct( measurement );

    kalmanFiltersMap[ KalmanManager::kalmanId ] = kalman;

    KalmanManager::kalmanId++;
}

void KalmanManager::updateKalman(int kalmanId, int current_x, int current_y)
{
    if ( kalmanFiltersMap.find(kalmanId) != kalmanFiltersMap.end() )
    {
        KalmanFilter kalman = kalmanFiltersMap[ kalmanId ];

        Mat_<float> measurement(2,1);
        measurement.setTo(Scalar(0));

        measurement(0) = current_x;
        measurement(1) = current_y;

        kalman.correct( measurement );
    }
    else
    {
        cout << "Kalman Id doesnot exists" << endl;
    }
}

void KalmanManager::deleteKalman(int kalmanId)
{
    kalmanFiltersMap.erase(kalmanId);
}

void KalmanManager::getTrackingKalmanPredictions(map<int, Mat> &kalmanPredictions)
{
    vector<int> outOfBoundKalman;

    map<int, KalmanFilter>::iterator iterator;

    for ( iterator = kalmanFiltersMap.begin(); iterator != kalmanFiltersMap.end(); ++iterator  )
    {
        int kalmanId = iterator->first;

        Mat kalmanPrediction = kalmanFiltersMap[ kalmanId ].predict();

        int kalmanX = int( kalmanPrediction.at<float>(0) );
        int kalmanY = int( kalmanPrediction.at<float>(1) );

        if( ( kalmanX + kalman_right_margin >= frameWidth ) || ( kalmanX - kalman_left_margin <= 0 ) || ( kalmanY + kalman_bottom_margin >= frameHeight ) )
        {
            outOfBoundKalman.push_back( kalmanId );
            continue;
        }

        kalmanPredictions[ kalmanId ] = kalmanPrediction;
    }

    reverse(outOfBoundKalman.begin(),outOfBoundKalman.end());

    for( int i = 0; i < outOfBoundKalman.size(); i++ )
    {
        kalmanFiltersMap.erase( outOfBoundKalman.at(i) );
    }
}

vector<KalmanContourMap> KalmanManager::mapKalmansToContours(map<int, Mat> kalmanPredictions, vector<Point2d> contourPositions)
{
    vector<KalmanContourMap> kalmanContourMapList;

    if(contourPositions.size() == 0)
    {
       return kalmanContourMapList;
    }
    else
    {
        map<int, Mat>::iterator iterator;
        for ( iterator = kalmanPredictions.begin(); iterator != kalmanPredictions.end(); ++iterator  )
        {
            KalmanContourMap kalmanContourMap;

            int kalmanId = iterator->first;

            kalmanContourMap.kalmanId = kalmanId;

            int kalmanX = int( kalmanPredictions[kalmanId].at<float>(0) );
            int kalmanY = int( kalmanPredictions[kalmanId].at<float>(1) );

            for(int j = 0; j < contourPositions.size(); j++ )
            {
                int actualX = contourPositions[j].x;
                int actualY = contourPositions[j].y;

                if(
                     ( ( kalmanX - kalman_left_margin ) < actualX ) &&
                     ( ( kalmanX + kalman_right_margin ) > actualX ) &&
                     ( ( kalmanY - kalman_top_margin ) < actualY ) &&
                     ( ( kalmanY + kalman_bottom_margin ) > actualY )
                  )
                {
                    double dx = kalmanX - actualX;
                    double dy = kalmanY - actualY;

                    double distance = sqrt( pow(dx, 2) + pow(dy, 2) );

                    kalmanContourMap.contourIndices.push_back( j );
                    kalmanContourMap.distances.push_back( distance );
                }
            }

            kalmanContourMapList.push_back(kalmanContourMap);
        }

        return kalmanContourMapList;
    }
}
