#include <iostream>
#include <dlib/opencv.h>
#include <dlib/image_processing.h>
#include <dlib/gui_widgets.h>
#include <dlib/image_io.h>
#include <dlib/dir_nav.h>
#include <ctime>
#include <algorithm>

#include "framegrabber.h"
#include "frameprocessor.h"
#include "kalmanmanager.h"

using namespace cv;
using namespace std;

#define FRAME_WIDTH 672
#define FRAME_HEIGHT 380

int main()
{
    FrameGrabber grabber(VIDEO_MODE, 0, "/home/prabu-test/PycharmProjects/objectCounting/src/samples/overlap_video_sample_1.avi");
    FrameProcessor frameProcessor(FRAME_WIDTH,FRAME_HEIGHT);
    KalmanManager kalmanManager(FRAME_WIDTH,FRAME_HEIGHT);

    Mat prevFrame;
    Mat currentFrame;
    Mat thresholdedFrame;
    Mat currentTrackingFrame;
    Mat dlibFrame;

    vector<Point2d> currentobjectCoordinates;
    vector<Point> currentobjectDimensions;

    vector<Point2d> prevUnmatchedobjectCoordinates;
    vector<Point> prevUnmatchedobjectDimensions;

    map<int, Mat> tempKalmanPredictions;
    vector<KalmanContourMap> tempKalmanContourMapList;

    int object_count = 0;
    int frameCount = 0;

    int64 start = cv::getTickCount();
    double millisecondsElapsed = 0;

    while( true )
    {
        currentobjectCoordinates.clear();
        currentobjectDimensions.clear();

        currentFrame = grabber.grabFrame();

        frameProcessor.getCurrentFrameobjectCoordinates(currentFrame, thresholdedFrame, currentobjectCoordinates, currentobjectDimensions);
        currentTrackingFrame = currentFrame.clone();
        dlibFrame = currentFrame.clone();

        #if DEBUG
            cout << "Prev unmatched coor size " << prevUnmatchedobjectCoordinates.size() << endl;
            cout << "current coor size " << currentobjectCoordinates.size() << endl;
        #endif

        // New objects found. Assign new trackers.
        if( prevUnmatchedobjectCoordinates.size() > 0 )
        {
            for (int i =0; i < prevUnmatchedobjectCoordinates.size(); i++)
            {
                dlib::correlation_tracker tracker;

                int x_centre = int( prevUnmatchedobjectCoordinates[i].x );
                int y_centre = int( prevUnmatchedobjectCoordinates[i].y );

                int width = prevUnmatchedobjectDimensions[i].x;
                int height = prevUnmatchedobjectDimensions[i].y;

                dlib::cv_image<dlib::bgr_pixel> prev_frame_dlib(prevFrame);
                tracker.start_track(prev_frame_dlib, dlib::rectangle( dlib::point(x_centre - width - 10, y_centre - height - 10), dlib::point(x_centre + width + 10, y_centre + height + 10 )));

                dlib::cv_image<dlib::bgr_pixel> current_frame_tracking_dlib(currentFrame);
                tracker.update(current_frame_tracking_dlib);

                dlib::rectangle rect = tracker.get_position();

                int dlib_x_centre = int( rect.left() + rect.right() ) / 2;
                int dlib_y_centre = int( rect.top() + rect.bottom() ) / 2 + 15;

            #if DEBUG
                cout << "dlib coordinates " << x_centre << "," << y_centre << "," << dlib_x_centre << "," << dlib_y_centre << endl;
            #endif

            #if ALL_FRAMES
                rectangle(dlibFrame, Point( x_centre - width, y_centre - height ), Point( x_centre + width, y_centre + height ), Scalar(255, 0, 0), 2);
            #endif

                kalmanManager.createNewKalman(x_centre, y_centre, dlib_x_centre, dlib_y_centre);

                object_count++;
            }

            #if ALL_FRAMES
                for(int i = 0; i < tempKalmanContourMapList.size(); i++ )
                {
                    int kalmanId = tempKalmanContourMapList.at(i).kalmanId;

                    int kalmanX = int( tempKalmanPredictions[kalmanId].at<float>(0) );
                    int kalmanY = int( tempKalmanPredictions[kalmanId].at<float>(1) );

                    rectangle(dlibFrame, Point( kalmanX - kalmanManager.kalman_left_margin , kalmanY - kalmanManager.kalman_top_margin ), Point( kalmanX + kalmanManager.kalman_right_margin, kalmanY + kalmanManager.kalman_bottom_margin ), Scalar(0, 0, 255), 2);
                    circle(dlibFrame, Point(kalmanX,kalmanY), 5, Scalar(0,255,0));
                }
            #endif

            #if ALL_FRAMES
                imshow("Dlib Frame", dlibFrame);
            #endif

            prevUnmatchedobjectCoordinates.clear();
        }
        else
        {
            map<int, Mat> kalmanPredictions;

            kalmanManager.getTrackingKalmanPredictions(kalmanPredictions);

            tempKalmanPredictions = kalmanPredictions;

            vector<KalmanContourMap> kalmanContourMapList = kalmanManager.mapKalmansToContours(kalmanPredictions, currentobjectCoordinates);

            tempKalmanContourMapList = kalmanContourMapList;

            vector<int> contoursTracked;

            for(int i = 0; i < kalmanContourMapList.size(); i++ )
            {
                #if ALL_FRAMES
                    int kalmanId = kalmanContourMapList.at(i).kalmanId;

                    int kalmanX = int( kalmanPredictions[kalmanId].at<float>(0) );
                    int kalmanY = int( kalmanPredictions[kalmanId].at<float>(1) );

                    rectangle(currentTrackingFrame, Point( kalmanX - kalmanManager.kalman_left_margin , kalmanY - kalmanManager.kalman_top_margin ), Point( kalmanX + kalmanManager.kalman_right_margin, kalmanY + kalmanManager.kalman_bottom_margin ), Scalar(0, 0, 255), 2);
                    circle(currentTrackingFrame, Point(kalmanX,kalmanY), 2, Scalar(0,255,0), -1 );
                    putText(currentTrackingFrame, to_string(kalmanId), Point( kalmanX - kalmanManager.kalman_left_margin - 5, kalmanY - kalmanManager.kalman_top_margin - 5 ), CV_FONT_BLACK, .5, Scalar(0, 255, 0));
                #endif

                if( kalmanContourMapList.at(i).distances.size() <= 0 )
                {
                    int kalmanId = kalmanContourMapList.at(i).kalmanId;
                    kalmanManager.deleteKalman( kalmanId );
                    continue;
                }

                int minimumDistanceIndex = distance(kalmanContourMapList.at(i).distances.begin(),min_element(kalmanContourMapList.at(i).distances.begin(),kalmanContourMapList.at(i).distances.end()));
                int closestContourIndex = kalmanContourMapList.at(i).contourIndices[minimumDistanceIndex];

                #if DEBUG
                    cout << "kalman to contour , " << kalmanContourMapList.at(i).kalmanId << "," << closestContourIndex << endl;
                    cout << kalmanContourMapList.at(i).kalmanId << "," << currentobjectCoordinates[closestContourIndex].x << "," << currentobjectCoordinates[closestContourIndex].y << endl;
                #endif

                kalmanManager.updateKalman(kalmanContourMapList.at(i).kalmanId, currentobjectCoordinates[closestContourIndex].x, currentobjectCoordinates[closestContourIndex].y + 5);

                if ( find(contoursTracked.begin(), contoursTracked.end(), closestContourIndex) == contoursTracked.end() )
                {
                    contoursTracked.push_back(closestContourIndex);
                }
            }

            sort(contoursTracked.begin(), contoursTracked.end(), [](const int a, const int b) {return a > b; });

            for( int i = 0; i < contoursTracked.size(); i++ )
            {
                currentobjectCoordinates.erase( currentobjectCoordinates.begin() + contoursTracked.at(i) );
                currentobjectDimensions.erase( currentobjectDimensions.begin() + contoursTracked.at(i) );
            }

            prevUnmatchedobjectCoordinates = currentobjectCoordinates;
            prevUnmatchedobjectDimensions = currentobjectDimensions;
        }

        prevFrame = currentFrame.clone();

        #if CURRENT_FRAME_TRACKING || ALL_FRAMES
            putText(currentTrackingFrame, to_string(object_count), Point(50, 50), CV_FONT_BLACK, 1, Scalar(0, 0, 255));
        #endif

        #if ALL_FRAMES
            imshow("Current Frame", currentFrame);
            imshow("Threshold Frame", thresholdedFrame);
        #endif

        #if CURRENT_FRAME_TRACKING || ALL_FRAMES
            imshow("Current Tracking Frame", currentTrackingFrame);
        #endif

        frameCount++;

        if( frameCount % 300 == 0)
        {
            millisecondsElapsed = (getTickCount() - start) / getTickFrequency();
            cout << "FPS : " << ( 300 / ( millisecondsElapsed ) ) << endl;
            start = getTickCount();
        }

        #if INTERACTIVE
            if(object_count < 1)
            {
                waitKey(1);
            }
            else
            {
                int key = waitKey(0);
                if( key == 113 || key == 27 )
                {
                    break;
                }
            }
        #elif CURRENT_FRAME_TRACKING || ALL_FRAMES
            int key = waitKey(1);
            if( key == 113 || key == 27 )
            {
                break;
            }
        #endif
    }
}
